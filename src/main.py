import falcon
import requests

from bs4 import BeautifulSoup


class RequestError(Exception):
    pass


class Index:
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200
        resp.content_type = 'text/html'
        with open('static/index.html', 'r') as f:
            resp.body = f.read()


class Analyze:
    def on_get(self, req, resp):
        url = req.params['url']
        try:
            resp.media = {'results': count_keywords(url)}
        except RequestError as e:
            resp.media = {'error': str(e)}
            resp.status = falcon.HTTP_400


def count_keywords(url):
    try:
        response = requests.get(url)
    except requests.exceptions.RequestException as e:
        raise RequestError(f'Request for page {url} failed (did you forgotten http(s):// prefix?)')
    if response.status_code != 200:
        raise RequestError(f'Request for page {url} failed')
    soup = BeautifulSoup(response.text, features='html.parser')
    try:
        raw_keywords = soup.find('meta', attrs={'name': 'keywords'})['content']
    except (KeyError, TypeError):
        raise RequestError('Failed to get keywords from meta tags')
    keywords = {kw.strip().lower() for kw in raw_keywords.split(',')}
    content = soup.body.get_text().lower()
    return [{'keyword': kw, 'count': content.count(kw)} for kw in keywords]


api = falcon.API()
api.add_route('/', Index())
api.add_route('/analyze', Analyze())


if __name__ == '__main__':
    api.run()
