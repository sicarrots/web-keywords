import falcon
import pytest
import requests

from falcon import testing
from main import api
from operator import itemgetter
from unittest.mock import Mock

valid_http_response = '''
<html>
<head>
<meta name="keywords" content="ala, pies"
</head>
<body>
Ala ma kota
Ala ma trzy koty
Ala ma psa
kot ma Alę
</body>
</html>
'''

no_keywords_http_response = '''
<html>
<head>
</head>
<body>
Ala ma kota
Ala ma trzy koty
Ala ma psa
kot ma Alę
</body>
</html>
'''


class FakeResponse:
    def __init__(self, status_code, text):
        self.status_code = status_code
        self.text = text


@pytest.fixture()
def client():
    return testing.TestClient(api)


def test_index_response(client):
    r = client.simulate_get('/')
    with open('static/index.html', 'r') as f:
        assert r.text == f.read()
    assert r.headers['content-type'] == 'text/html'


def test_valid_response(client, monkeypatch):
    get_mock = Mock(return_value=FakeResponse(200, valid_http_response))
    monkeypatch.setattr('main.requests.get', get_mock)
    r = client.simulate_get('/analyze', params=dict(url='http://foo.bar'))
    assert r.status_code == 200
    assert sorted(r.json['results'], key=itemgetter('keyword')) == \
           [{'keyword': 'ala', 'count': 3}, {'keyword': 'pies', 'count': 0}]


def test_request_for_invalid_url(client, monkeypatch):
    get_mock = Mock(side_effect=requests.exceptions.RequestException)
    monkeypatch.setattr('main.requests.get', get_mock)
    r = client.simulate_get('/analyze', params=dict(url='http://foo.bar'))
    assert r.status_code == 400
    assert r.json == {'error': 'Request for page http://foo.bar failed (did you forgotten http(s):// prefix?)'}


def test_request_with_non_200_status(client, monkeypatch):
    get_mock = Mock(return_value=FakeResponse(400, ''))
    monkeypatch.setattr('main.requests.get', get_mock)
    r = client.simulate_get('/analyze', params=dict(url='http://foo.bar'))
    assert r.status_code == 400
    assert r.json == {'error': 'Request for page http://foo.bar failed'}


def test_no_keywords_meta_tag(client, monkeypatch):
    get_mock = Mock(return_value=FakeResponse(200, no_keywords_http_response))
    monkeypatch.setattr('main.requests.get', get_mock)
    r = client.simulate_get('/analyze', params=dict(url='http://foo.bar'))
    assert r.status_code == 400
    assert r.json == {'error': 'Failed to get keywords from meta tags'}
