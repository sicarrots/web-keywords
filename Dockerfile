FROM python:3-stretch

ENV PORT 8000
EXPOSE 8000
WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
COPY src /app

CMD ["gunicorn", "main:api"]