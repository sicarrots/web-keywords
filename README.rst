Building docker image
=====================
``docker build . -t web-keywords``

Running
=======
``docker run -it -p 8000:8000 web-keywords``
Open web browser at http://0.0.0.0:8000

Running tests
=============
``docker run -it web-keywords pytest /app/test_main.py``